/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package edu.umass.cs.client;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int greend=0x7f020000;
        public static final int icon=0x7f020001;
    }
    public static final class id {
        public static final int AccelLayout=0x7f050009;
        public static final int AccelXView=0x7f05000b;
        public static final int AccelYView=0x7f05000d;
        public static final int AccelZView=0x7f05000f;
        public static final int StartButton=0x7f050002;
        public static final int StartButtonLayout=0x7f050001;
        public static final int StatusLayout=0x7f050007;
        public static final int StatusView=0x7f050008;
        public static final int StepCountView=0x7f050004;
        public static final int StepsLayout=0x7f050003;
        public static final int TopHorizontalSpaceView=0x7f050000;
        public static final int XView=0x7f05000a;
        public static final int YView=0x7f05000c;
        public static final int ZView=0x7f05000e;
        public static final int activityLayout=0x7f050005;
        public static final int activityView=0x7f050006;
    }
    public static final class layout {
        public static final int main=0x7f030000;
    }
    public static final class string {
        public static final int accel=0x7f040000;
        public static final int activity=0x7f040001;
        public static final int app_name=0x7f040002;
        public static final int nullvalue=0x7f040003;
        public static final int status=0x7f040004;
        public static final int steps=0x7f040005;
        public static final int ui_collector_button_delete_title=0x7f040006;
        public static final int ui_collector_button_start_title=0x7f040007;
        public static final int ui_collector_button_stop_title=0x7f040008;
        public static final int ui_collector_radio_other_title=0x7f040009;
        public static final int ui_collector_radio_running_title=0x7f04000a;
        public static final int ui_collector_radio_sitting_title=0x7f04000b;
        public static final int ui_collector_radio_standing_title=0x7f04000c;
        public static final int ui_collector_radio_walking_title=0x7f04000d;
        public static final int ui_collector_toast_file_deleted=0x7f04000e;
        public static final int ui_sensor_service_notification_content=0x7f04000f;
        public static final int ui_sensor_service_notification_ticker=0x7f040010;
        public static final int ui_sensor_service_notification_title=0x7f040011;
        public static final int ui_sensor_service_toast_error_file_saving_failed=0x7f040012;
        public static final int ui_sensor_service_toast_success_file_created=0x7f040013;
        public static final int ui_sensor_service_toast_success_file_updated=0x7f040014;
        public static final int x=0x7f040015;
        public static final int y=0x7f040016;
        public static final int z=0x7f040017;
    }
}
