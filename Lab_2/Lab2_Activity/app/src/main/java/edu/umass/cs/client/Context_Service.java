package edu.umass.cs.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.CalendarContract;
import android.util.Log;
import android.widget.Toast;

import com.meapsoft.FFT;

import edu.dartmouth.cs.myrunscollector.Globals;
import edu.umass.cs.accelerometer.Filter;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * 
 * Context_Service: This is a sample class to reads sensor data (accelerometer). 
 * 
 * @author CS390MB
 * 
 */
public class Context_Service extends Service implements SensorEventListener{



	/**
	 * Notification manager to display notifications
	 */
	private NotificationManager nm;
	
	/**
	 * SensorManager
	 */
    private File mRawFile;
    private FileOutputStream mRawOutputStream;
	private SensorManager mSensorManager;
    /**
     * Accelerometer Sensor
     */
    private Sensor mAccelerometer;

	//List of bound clients/activities to this service
	ArrayList<Messenger> mClients = new ArrayList<Messenger>();

	//Message codes sent and received by the service
	static final int MSG_REGISTER_CLIENT = 1;
	static final int MSG_UNREGISTER_CLIENT = 2;
	static final int MSG_ACTIVITY_STATUS = 3;
	static final int MSG_STEP_COUNTER = 4;
	static final int MSG_ACCEL_VALUES = 5;
	static final int MSG_START_ACCELEROMETER = 6;
	static final int MSG_STOP_ACCELEROMETER = 7;
	static final int MSG_ACCELEROMETER_STARTED = 8;
	static final int MSG_ACCELEROMETER_STOPPED = 9;

	static Context_Service sInstance = null;
	private static boolean isRunning = false;
	private static boolean isAccelRunning = false;
	private static final int NOTIFICATION_ID = 777;


    LinkedList<Double> exp=new LinkedList();
    LinkedList<Double> magn=new LinkedList();
    final double alpha=0.0456;
    final int winSize=100;
	
	/**
	 * Filter class required to filter noise from accelerometer
	 */
	private Filter filter = null;
	/**
	 * Step count to be displayed in UI
	 */
	private int stepCount = 0;
	
	//Messenger used by clients
	final Messenger mMessenger = new Messenger(new IncomingHandler());

    // second assignment

    private Instances mDataset;
    private static final int mFeatLen = Globals.ACCELEROMETER_BLOCK_CAPACITY + 1;
    private Attribute mClassAttribute;
    private String mLabel;
    private int mServiceTaskType;
    private static ArrayBlockingQueue<Double> mAccBuffer;
    private File mFeatureFile;
    private OnSensorChangedTask mAsyncTask;
	/**
	 * Handler to handle incoming messages
	 */
	@SuppressLint("HandlerLeak")
	class IncomingHandler extends Handler { 
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				mClients.remove(msg.replyTo);
				break;
			case MSG_START_ACCELEROMETER:
			{
				isAccelRunning = true;
				mSensorManager.registerListener(sInstance, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
				sendMessageToUI(MSG_ACCELEROMETER_STARTED);
				showNotification();
				//Set up filter
				//Following sets up smoothing filter from mcrowdviz
				int SMOOTH_FACTOR = 10;
				filter = new Filter(SMOOTH_FACTOR);
				//OR Use Butterworth filter from mcrowdviz
				//double CUTOFF_FREQUENCY = 0.3;
				//filter = new Filter(CUTOFF_FREQUENCY);
				stepCount = 0;
				break;
			}
			case MSG_STOP_ACCELEROMETER:
			{
				isAccelRunning = false;
				mSensorManager.unregisterListener(sInstance);
				sendMessageToUI(MSG_ACCELEROMETER_STOPPED);
				showNotification();
				//Free filter and step detector
				filter = null;
				break;
			}
			default:
				super.handleMessage(msg);
			}
		}
	}


	private void sendMessageToUI(int message) {
		for (int i=mClients.size()-1; i>=0; i--) {
			try {
				// Send message value
				mClients.get(i).send(Message.obtain(null, message));
			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}
	
	private void sendAccelValuesToUI(float accX, float accY, float accZ) {
		for (int i=mClients.size()-1; i>=0; i-- ) {
			try {
				
				//Send Accel Values
				Bundle b = new Bundle();
				b.putFloat("accx", accX);
				b.putFloat("accy", accY);
				b.putFloat("accz", accZ);
				Message msg = Message.obtain(null, MSG_ACCEL_VALUES);
				msg.setData(b);
				mClients.get(i).send(msg);


			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}
	
	private void sendUpdatedStepCountToUI() {
		for (int i=mClients.size()-1; i>=0; i-- ) {
			try {
				//Send Step Count
				Message msg = Message.obtain(null, MSG_STEP_COUNTER,stepCount,0);
				mClients.get(i).send(msg);

			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}
	
	

	/**
	 * On Binding, return a binder
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}


	
	

	//Start service automatically if we reboot the phone
	public static class Context_BGReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Intent bootUp = new Intent(context,Context_Service.class);
			context.startService(bootUp);
		}		
	}

	@SuppressWarnings("deprecation")
	private void showNotification() {
		//Cancel previous notification
		if(nm!=null)
			nm.cancel(NOTIFICATION_ID);
		else
			nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

		// The PendingIntent to launch our activity if the user selects this notification
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);

		// Use the commented block of code if your target environment is Android-16 or higher 
		/*Notification notification = new Notification.Builder(this)
		.setContentTitle("Context Service")
		.setContentText("Running").setSmallIcon(R.drawable.icon)
		.setContentIntent(contentIntent)
		.build();
		
		nm.notify(NOTIFICATION_ID, notification); */
		
		//For lower versions of Android, the following code should work
		Notification notification = new Notification();
		notification.icon = R.drawable.icon;
		notification.tickerText = getString(R.string.app_name);
		notification.contentIntent = contentIntent;
        notification.when = System.currentTimeMillis();
        if(isAccelerometerRunning())
        	notification.setLatestEventInfo(getApplicationContext(), getString(R.string.app_name), "Accelerometer Running", contentIntent);
        else
        	notification.setLatestEventInfo(getApplicationContext(), getString(R.string.app_name), "Accelerometer Not Started", contentIntent);
        
        // Send the notification.
        nm.notify(NOTIFICATION_ID, notification);
	}

	/* getInstance() and isRunning() are required by the */
	static Context_Service getInstance(){
		return sInstance;
	}

	protected static boolean isRunning(){
		return isRunning;
	}
	
	protected static boolean isAccelerometerRunning() {
		return isAccelRunning;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		showNotification();
		isRunning = true;
		sInstance = this;
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mAccBuffer = new ArrayBlockingQueue<Double>(
                Globals.ACCELEROMETER_BUFFER_CAPACITY);

        mAsyncTask = new OnSensorChangedTask();
        mAsyncTask.execute();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		nm.cancel(NOTIFICATION_ID); // Cancel the persistent notification.
        isRunning = false;
		//Don't let Context_Service die!
		Intent mobilityIntent = new Intent(this,Context_Service.class);
		startService(mobilityIntent);
	}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

       /* mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        mSensorManager.registerListener(this, mAccelerometer,
                SensorManager.SENSOR_DELAY_FASTEST);

        /*Bundle extras = intent.getExtras();
        mLabel = extras.getString(Globals.CLASS_LABEL_KEY);

        // mFeatureFile = new File(getExternalFilesDir(null), Globals.FEATURE_FILE_NAME);
        mFeatureFile = new File(Environment.getExternalStorageDirectory(), Globals.FEATURE_FILE_NAME);

        Log.d("absolute path:", mFeatureFile.getAbsolutePath());

        mRawFile = new File(getExternalFilesDir(null), "acc_raw_" + mLabel + "_" +
                System.currentTimeMillis() / 1000 + ".csv");
        try {
            mRawOutputStream = new FileOutputStream(mRawFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.d(Globals.TAG, mRawFile.getAbsolutePath());

        mServiceTaskType = Globals.SERVICE_TASK_TYPE_COLLECT;*/

        // Create the container for attributes
        ArrayList<Attribute> allAttr = new ArrayList<Attribute>();

        // Adding FFT coefficient attributes
        DecimalFormat df = new DecimalFormat("0000");

        for (int i = 0; i < Globals.ACCELEROMETER_BLOCK_CAPACITY; i++) {
            allAttr.add(new Attribute(Globals.FEAT_FFT_COEF_LABEL + df.format(i)));
        }
        // Adding the max feature
        allAttr.add(new Attribute(Globals.FEAT_MAX_LABEL));

        // Declare a nominal attribute along with its candidate values
        ArrayList<String> labelItems = new ArrayList<String>(3);
        labelItems.add(Globals.CLASS_LABEL_STANDING);
        labelItems.add(Globals.CLASS_LABEL_WALKING);
        labelItems.add(Globals.CLASS_LABEL_RUNNING);
        labelItems.add(Globals.CLASS_LABEL_SITTING);
        labelItems.add(Globals.CLASS_LABEL_OTHER);
        mClassAttribute = new Attribute(Globals.CLASS_LABEL_KEY, labelItems);
        allAttr.add(mClassAttribute);

        // Construct the dataset with the attributes specified as allAttr and
        // capacity 10000
        mDataset = new Instances(Globals.FEAT_SET_NAME, allAttr, Globals.FEATURE_SET_CAPACITY);

        // Set the last column/attribute (standing/walking/running) as the class
        // index for classification
        mDataset.setClassIndex(mDataset.numAttributes() - 1);



        mAsyncTask = new OnSensorChangedTask();
        mAsyncTask.execute();

        return START_NOT_STICKY;
    }
    /*
	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        ArrayList<Attribute> allAttr = new ArrayList<Attribute>();
        // Declare a nominal attribute along with its candidate values
        ArrayList<String> labelItems = new ArrayList<String>();
        labelItems.add(Globals.CLASS_LABEL_STANDING);
        labelItems.add(Globals.CLASS_LABEL_WALKING);
        labelItems.add(Globals.CLASS_LABEL_RUNNING);
        labelItems.add(Globals.CLASS_LABEL_SITTING);
        labelItems.add(Globals.CLASS_LABEL_OTHER);
        Log.d("on start","on start");
        mClassAttribute = new Attribute(Globals.CLASS_LABEL_KEY, labelItems);
        allAttr.add(mClassAttribute);


        mLabel = mClassAttribute.toString();

        // capacity 10000
        mDataset = new Instances(Globals.FEAT_SET_NAME, allAttr, Globals.FEATURE_SET_CAPACITY);

        // Set the last column/attribute (standing/walking/running) as the class
        // index for classification
        mDataset.setClassIndex(mDataset.numAttributes() - 1);

        return START_STICKY; // run until explicitly stopped.
    }*/


	/* (non-Javadoc)
	 * @see android.hardware.SensorEventListener#onAccuracyChanged(android.hardware.Sensor, int)
	 */
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		
	}

	/* (non-Javadoc)
	 * @see android.hardware.SensorEventListener#onSensorChanged(android.hardware.SensorEvent)
	 */
	@Override
	public void onSensorChanged(SensorEvent event) {
		if(event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
			
			float accel[] = event.values;
			sendAccelValuesToUI(accel[0], accel[1], accel[2]);
			
			
			/**
			 * TODO: Step Detection
			 */
			//First, Get filtered values
			//double filtAcc[] = filter.getFilteredValues(accel[0], accel[1], accel[2]);

            magn.add(Magnitude(accel[0], accel[1], accel[2]));


            exp.add(exponentialSmooth());

            if(magn.size()==winSize)
            {
                double max=getMax(exp);
                double min=getMin(exp);
                if((max-min)>=2)
                {
                    double ddt=(max+min)/2;
                    detectStep(ddt);
                }

                exp.clear();
                magn.clear();
            }

			sendUpdatedStepCountToUI();
			
		}

	}
	
	/**
	 * This should return number of steps detected.
	 * @param filt_acc_x
	 * @param filt_acc_y
	 * @param filt_acc_z
	 * @return
	 */
	public int detectSteps(double filt_acc_x, double filt_acc_y, double filt_acc_z) {
		return 0;
	}

    public double Magnitude(float x,float y,float z)
    {
        double m=Math.sqrt(Math.pow(x,2)+Math.pow(y,2)+Math.pow(z,2));
        try {
            mAccBuffer.add(new Double(m));
        } catch (IllegalStateException e) {

            // Exception happens when reach the capacity.
            // Doubling the buffer. ListBlockingQueue has no such issue,
            // But generally has worse performance
            ArrayBlockingQueue<Double> newBuf = new ArrayBlockingQueue<Double>(
                    mAccBuffer.size() * 2);

            mAccBuffer.drainTo(newBuf);
            mAccBuffer = newBuf;
            mAccBuffer.add(new Double(m));
        }
        //Log.d("magn:::",""+m);
        return  m;
    }

    public double exponentialSmooth()
    {
        double lastMagn=0;
        if(magn.size()==1)
            return 0.0;
        else if(magn.size()==2)
            return magn.get(magn.size()-2);
        else if(magn.size()>2)
            lastMagn=magn.get(magn.size()-2);
        else
            lastMagn=magn.get(0);

        double lastExp=exp.getLast();
        double sm=alpha*lastMagn+(1-alpha)*lastExp;
        //Log.d("smoothing:::", "" + sm);
        return sm;
    }

    public void detectStep(double ddt)
    {
        for(int k=0;k<exp.size()-1;k++)
        {
            double y1=exp.get(k);
            double y2=exp.get(k+1);
            if(y2<ddt && y1>=ddt) {
                stepCount = stepCount + 1;
            }
        }
    }

    public double getMax(LinkedList<Double> maxList)
    {
        return Collections.max(maxList);
    }
    public double getMin(LinkedList<Double> minList)
    {
        return Collections.min(minList);
    }

    private class OnSensorChangedTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... arg0) {

            Instance inst = new DenseInstance(mFeatLen);
            inst.setDataset(mDataset);
            int blockSize = 0;
            FFT fft = new FFT(Globals.ACCELEROMETER_BLOCK_CAPACITY);
            double[] accBlock = new double[Globals.ACCELEROMETER_BLOCK_CAPACITY];
            double[] re = accBlock;
            double[] im = new double[Globals.ACCELEROMETER_BLOCK_CAPACITY];

            double max = Double.MIN_VALUE;

            while (true) {
                try {
                    // need to check if the AsyncTask is cancelled or not in the while loop
                    if (isCancelled() == true) {
                        return null;
                    }
                    //Log.d(mAccBuffer.take().doubleValue()+"::block::",""+blockSize);
                    // Dumping buffer
                    accBlock[blockSize++] = mAccBuffer.take().doubleValue();

                    if (blockSize == Globals.ACCELEROMETER_BLOCK_CAPACITY) {
                        blockSize = 0;

                        // time = System.currentTimeMillis();
                        max = .0;
                        for (double val : accBlock) {
                            if (max < val) {
                                max = val;
                            }
                        }

                        fft.fft(re, im);

                        for (int i = 0; i < re.length; i++) {
                            double mag = Math.sqrt(re[i] * re[i] + im[i]
                                    * im[i]);
                            inst.setValue(i, mag);
                            im[i] = .0; // Clear the field
                        }

                        // Append max after frequency component
                        inst.setValue(Globals.ACCELEROMETER_BLOCK_CAPACITY-1, max);

                        //inst.setValue(mClassAttribute, mLabel);
                        mDataset.add(inst);

                        Double [] dataSet=convertInstance(inst);

                        try
                        {
                            double r=WekaClassifier.classify(dataSet);

                            Log.d("value of r:::",""+r);
                            sendActivityToUI(r);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                        /*Log.d(inst.numValues()+"::Instance:::",""+inst.attribute(2).toString());
                        WekaWrapper w=new WekaWrapper();
                        w.classifyInstance(inst);*/
                       // Log.d("new instance::", mDataset.size() + "");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Double [] convertInstance(Instance instance)
    {
        double [] t=instance.toDoubleArray();
        Double [] instnce=new Double[t.length];

        for(int i=0;i<t.length;i++)
        {
            instnce[i]=Double.valueOf(t[i]);
        }
        return  instnce;
    }
// display the corresponding activity of the UI
    private void sendActivityToUI(double r) {
        if (r == 0.0) {
            MainActivity.activityView.setText("Standing");
        }
        else if (r == 1.0) {
            MainActivity.activityView.setText("Walking");
        }
        else if (r == 2.0) {
            MainActivity.activityView.setText("Running");
        }
        else if (r == 3.0) {
            MainActivity.activityView.setText("Sitting");
        } else {
            MainActivity.activityView.setText("Still");
        }
    }
    }





