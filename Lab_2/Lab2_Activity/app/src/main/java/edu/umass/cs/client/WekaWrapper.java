/*
=== Detailed Accuracy By Class ===

               TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
                 0.945     0.007      0.983     0.945     0.964      0.996    standing
                 0.987     0.028      0.942     0.987     0.964      0.99     walking
                 0.941     0.006      0.941     0.941     0.941      0.994    running
                 0.953     0.014      0.964     0.953     0.958      0.993    sitting
                 0         0          0         0         0          ?        others
Weighted Avg.    0.96      0.016      0.961     0.96      0.96       0.993

=== Confusion Matrix ===

   a   b   c   d   e   <-- classified as
 292   6   1  10   0 |   a = standing
   0 306   4   0   0 |   b = walking
   0   5  80   0   0 |   c = running
   5   8   0 264   0 |   d = sitting
   0   0   0   0   0 |   e = others
* */

// Generated with Weka 3.7.4
//
// This code is public domain and comes with no warranty.
//
// Timestamp: Sat Sep 26 18:10:58 EET 2015

package edu.umass.cs.client;

import weka.core.Attribute;
import weka.core.Capabilities;
import weka.core.Capabilities.Capability;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.RevisionUtils;
import weka.classifiers.Classifier;
import weka.classifiers.AbstractClassifier;

public class WekaWrapper
        extends AbstractClassifier {

    /**
     * Returns only the toString() method.
     *
     * @return a string describing the classifier
     */
    public String globalInfo() {
        return toString();
    }

    /**
     * Returns the capabilities of this classifier.
     *
     * @return the capabilities
     */
    public Capabilities getCapabilities() {
        weka.core.Capabilities result = new weka.core.Capabilities(this);

        result.enable(weka.core.Capabilities.Capability.NOMINAL_ATTRIBUTES);
        result.enable(weka.core.Capabilities.Capability.NUMERIC_ATTRIBUTES);
        result.enable(weka.core.Capabilities.Capability.DATE_ATTRIBUTES);
        result.enable(weka.core.Capabilities.Capability.MISSING_VALUES);
        result.enable(weka.core.Capabilities.Capability.NOMINAL_CLASS);
        result.enable(weka.core.Capabilities.Capability.MISSING_CLASS_VALUES);


        result.setMinimumNumberInstances(0);

        return result;
    }

    /**
     * only checks the data against its capabilities.
     *
     * @param i the training data
     */
    public void buildClassifier(Instances i) throws Exception {
        // can classifier handle the data?
        getCapabilities().testWithFail(i);
    }

    /**
     * Classifies the given instance.
     *
     * @param i the instance to classify
     * @return the classification result
     */
    public double classifyInstance(Instance i) throws Exception {
        Object[] s = new Object[i.numAttributes()];

        for (int j = 0; j < s.length; j++) {
            if (!i.isMissing(j)) {
                if (i.attribute(j).isNominal())
                    s[j] = new String(i.stringValue(j));
                else if (i.attribute(j).isNumeric())
                    s[j] = new Double(i.value(j));
            }
        }

        // set class value to missing
        s[i.classIndex()] = null;

        return WekaClassifier.classify(s);
    }

    /**
     * Returns the revision string.
     *
     * @return        the revision
     */
    public String getRevision() {
        return RevisionUtils.extract("1.0");
    }

    /**
     * Returns only the classnames and what classifier it is based on.
     *
     * @return a short description
     */
    public String toString() {
        return "Auto-generated classifier wrapper, based on weka.classifiers.trees.J48 (generated with Weka 3.7.4).\n" + this.getClass().getName() + "/WekaClassifier";
    }

    /**
     * Runs the classfier from commandline.
     *
     * @param args the commandline arguments
     */
    public static void main(String args[]) {
        runClassifier(new WekaWrapper(), args);
    }
}

class WekaClassifier {

    public static double classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = WekaClassifier.Nd5bbc70(i);
        return p;
    }
    static double Nd5bbc70(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 91.480788) {
            p = WekaClassifier.N5649fd1(i);
        } else if (((Double) i[0]).doubleValue() > 91.480788) {
            p = WekaClassifier.N877eb617(i);
        }
        return p;
    }
    static double N5649fd1(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 3;
        } else if (((Double) i[0]).doubleValue() <= 27.005373) {
            p = WekaClassifier.Na16fce2(i);
        } else if (((Double) i[0]).doubleValue() > 27.005373) {
            p = WekaClassifier.N1eac37e4(i);
        }
        return p;
    }
    static double Na16fce2(Object []i) {
        double p = Double.NaN;
        if (i[14] == null) {
            p = 3;
        } else if (((Double) i[14]).doubleValue() <= 0.870056) {
            p = 3;
        } else if (((Double) i[14]).doubleValue() > 0.870056) {
            p = WekaClassifier.Nf840a53(i);
        }
        return p;
    }
    static double Nf840a53(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() <= 1.989382) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() > 1.989382) {
            p = 3;
        }
        return p;
    }
    static double N1eac37e4(Object []i) {
        double p = Double.NaN;
        if (i[28] == null) {
            p = 0;
        } else if (((Double) i[28]).doubleValue() <= 0.430578) {
            p = WekaClassifier.N15d739f5(i);
        } else if (((Double) i[28]).doubleValue() > 0.430578) {
            p = WekaClassifier.N1471ae712(i);
        }
        return p;
    }
    static double N15d739f5(Object []i) {
        double p = Double.NaN;
        if (i[24] == null) {
            p = 0;
        } else if (((Double) i[24]).doubleValue() <= 0.146456) {
            p = WekaClassifier.N83ae256(i);
        } else if (((Double) i[24]).doubleValue() > 0.146456) {
            p = WekaClassifier.N102ea698(i);
        }
        return p;
    }
    static double N83ae256(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 33.471847) {
            p = WekaClassifier.Ne026b87(i);
        } else if (((Double) i[0]).doubleValue() > 33.471847) {
            p = 0;
        }
        return p;
    }
    static double Ne026b87(Object []i) {
        double p = Double.NaN;
        if (i[18] == null) {
            p = 0;
        } else if (((Double) i[18]).doubleValue() <= 0.243364) {
            p = 0;
        } else if (((Double) i[18]).doubleValue() > 0.243364) {
            p = 3;
        }
        return p;
    }
    static double N102ea698(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 36.457187) {
            p = WekaClassifier.N1c3ac9d9(i);
        } else if (((Double) i[0]).doubleValue() > 36.457187) {
            p = WekaClassifier.Nc6d4c610(i);
        }
        return p;
    }
    static double N1c3ac9d9(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() <= 1.505899) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() > 1.505899) {
            p = 3;
        }
        return p;
    }
    static double Nc6d4c610(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() <= 3.994514) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() > 3.994514) {
            p = WekaClassifier.N166f58011(i);
        }
        return p;
    }
    static double N166f58011(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 3;
        } else if (((Double) i[6]).doubleValue() <= 4.472202) {
            p = 3;
        } else if (((Double) i[6]).doubleValue() > 4.472202) {
            p = 0;
        }
        return p;
    }
    static double N1471ae712(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 3;
        } else if (((Double) i[6]).doubleValue() <= 13.019192) {
            p = WekaClassifier.N19aeb9613(i);
        } else if (((Double) i[6]).doubleValue() > 13.019192) {
            p = 1;
        }
        return p;
    }
    static double N19aeb9613(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 3;
        } else if (((Double) i[2]).doubleValue() <= 14.443435) {
            p = WekaClassifier.N15c724714(i);
        } else if (((Double) i[2]).doubleValue() > 14.443435) {
            p = WekaClassifier.N79e5b416(i);
        }
        return p;
    }
    static double N15c724714(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 3;
        } else if (((Double) i[5]).doubleValue() <= 4.343248) {
            p = 3;
        } else if (((Double) i[5]).doubleValue() > 4.343248) {
            p = WekaClassifier.N144441915(i);
        }
        return p;
    }
    static double N144441915(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 19.75128) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() > 19.75128) {
            p = 3;
        }
        return p;
    }
    static double N79e5b416(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 1;
        } else if (((Double) i[1]).doubleValue() <= 18.659566) {
            p = 1;
        } else if (((Double) i[1]).doubleValue() > 18.659566) {
            p = 3;
        }
        return p;
    }
    static double N877eb617(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 369.133697) {
            p = WekaClassifier.Ne3fb6b18(i);
        } else if (((Double) i[0]).doubleValue() > 369.133697) {
            p = WekaClassifier.N13683534(i);
        }
        return p;
    }
    static double Ne3fb6b18(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() <= 8.187407) {
            p = WekaClassifier.N13e076219(i);
        } else if (((Double) i[7]).doubleValue() > 8.187407) {
            p = WekaClassifier.Nd2270726(i);
        }
        return p;
    }
    static double N13e076219(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 3;
        } else if (((Double) i[64]).doubleValue() <= 2.870472) {
            p = 3;
        } else if (((Double) i[64]).doubleValue() > 2.870472) {
            p = WekaClassifier.N93153420(i);
        }
        return p;
    }
    static double N93153420(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 1;
        } else if (((Double) i[6]).doubleValue() <= 14.808047) {
            p = WekaClassifier.N1d4b5de21(i);
        } else if (((Double) i[6]).doubleValue() > 14.808047) {
            p = 1;
        }
        return p;
    }
    static double N1d4b5de21(Object []i) {
        double p = Double.NaN;
        if (i[12] == null) {
            p = 2;
        } else if (((Double) i[12]).doubleValue() <= 3.179483) {
            p = WekaClassifier.N57342c22(i);
        } else if (((Double) i[12]).doubleValue() > 3.179483) {
            p = WekaClassifier.N1bf2b1323(i);
        }
        return p;
    }
    static double N57342c22(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() <= 14.382141) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() > 14.382141) {
            p = 2;
        }
        return p;
    }
    static double N1bf2b1323(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 1;
        } else if (((Double) i[6]).doubleValue() <= 13.019192) {
            p = WekaClassifier.N1f9b46524(i);
        } else if (((Double) i[6]).doubleValue() > 13.019192) {
            p = 2;
        }
        return p;
    }
    static double N1f9b46524(Object []i) {
        double p = Double.NaN;
        if (i[24] == null) {
            p = 1;
        } else if (((Double) i[24]).doubleValue() <= 1.371362) {
            p = 1;
        } else if (((Double) i[24]).doubleValue() > 1.371362) {
            p = WekaClassifier.N1f7948125(i);
        }
        return p;
    }
    static double N1f7948125(Object []i) {
        double p = Double.NaN;
        if (i[12] == null) {
            p = 3;
        } else if (((Double) i[12]).doubleValue() <= 10.540963) {
            p = 3;
        } else if (((Double) i[12]).doubleValue() > 10.540963) {
            p = 1;
        }
        return p;
    }
    static double Nd2270726(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 1;
        } else if (((Double) i[1]).doubleValue() <= 46.083328) {
            p = WekaClassifier.N4b783d27(i);
        } else if (((Double) i[1]).doubleValue() > 46.083328) {
            p = 1;
        }
        return p;
    }
    static double N4b783d27(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 327.63736) {
            p = WekaClassifier.N27ba8d28(i);
        } else if (((Double) i[0]).doubleValue() > 327.63736) {
            p = WekaClassifier.Naa8da233(i);
        }
        return p;
    }
    static double N27ba8d28(Object []i) {
        double p = Double.NaN;
        if (i[19] == null) {
            p = 1;
        } else if (((Double) i[19]).doubleValue() <= 8.862717) {
            p = WekaClassifier.N147771429(i);
        } else if (((Double) i[19]).doubleValue() > 8.862717) {
            p = WekaClassifier.N193810531(i);
        }
        return p;
    }
    static double N147771429(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 1;
        } else if (((Double) i[5]).doubleValue() <= 10.943958) {
            p = WekaClassifier.N6086e430(i);
        } else if (((Double) i[5]).doubleValue() > 10.943958) {
            p = 1;
        }
        return p;
    }
    static double N6086e430(Object []i) {
        double p = Double.NaN;
        if (i[15] == null) {
            p = 1;
        } else if (((Double) i[15]).doubleValue() <= 6.663791) {
            p = 1;
        } else if (((Double) i[15]).doubleValue() > 6.663791) {
            p = 0;
        }
        return p;
    }
    static double N193810531(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 0;
        } else if (((Double) i[7]).doubleValue() <= 17.506903) {
            p = 0;
        } else if (((Double) i[7]).doubleValue() > 17.506903) {
            p = WekaClassifier.N49b22832(i);
        }
        return p;
    }
    static double N49b22832(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 3;
        } else if (((Double) i[3]).doubleValue() <= 31.281356) {
            p = 3;
        } else if (((Double) i[3]).doubleValue() > 31.281356) {
            p = 1;
        }
        return p;
    }
    static double Naa8da233(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 344.035732) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 344.035732) {
            p = 2;
        }
        return p;
    }
    static double N13683534(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() <= 87.0563) {
            p = WekaClassifier.N187539b35(i);
        } else if (((Double) i[1]).doubleValue() > 87.0563) {
            p = WekaClassifier.Na871b136(i);
        }
        return p;
    }
    static double N187539b35(Object []i) {
        double p = Double.NaN;
        if (i[26] == null) {
            p = 2;
        } else if (((Double) i[26]).doubleValue() <= 12.468019) {
            p = 2;
        } else if (((Double) i[26]).doubleValue() > 12.468019) {
            p = 1;
        }
        return p;
    }
    static double Na871b136(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 458.047795) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() > 458.047795) {
            p = WekaClassifier.N9d3d9b37(i);
        }
        return p;
    }
    static double N9d3d9b37(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 499.534016) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 499.534016) {
            p = 2;
        }
        return p;
    }
}
